# Configuration Docker Compose

Ce fichier `docker-compose.yml` décrit l'environnement Docker utilisé pour exécuter plusieurs services, notamment un serveur web, un serveur de base de données PostgreSQL, pgAdmin et un serveur Minetest.

## Services

### Serveur Web (Apache)

- **Image Docker :** httpd
- **Ports :** Le port 80 du conteneur est exposé sur le port 80 de l'hôte.
- **Volumes :** Le répertoire local `./site` est monté dans le répertoire `/usr/local/apache2/htdocs` du conteneur, permettant ainsi de servir le site web contenu dans ce répertoire.

### Serveur de Base de Données PostgreSQL

- **Image Docker :** postgres
- **Ports :** Le port 5432 du conteneur est exposé sur le port 8181 de l'hôte.
- **Variables d'environnement :** Le mot de passe et le nom d'utilisateur de la base de données sont définis.
- **Volumes :** Un volume nommé `db_data` est utilisé pour stocker les données de la base de données PostgreSQL.
- **Réseau :** Ce service est connecté au réseau interne `backend`.

### pgAdmin

- **Image Docker :** elestio/pgadmin
- **Variables d'environnement :** L'adresse e-mail et le mot de passe par défaut pour pgAdmin sont définis.
- **Réseau :** Ce service est également connecté au réseau interne `backend`.

### Serveur Minetest

- **Image Docker :** linuxserver/minetest
- **Ports :** Le port 35565 du conteneur est exposé sur le port 35565 de l'hôte.
- **Variables d'environnement :** Diverses variables sont définies pour la configuration du serveur Minetest, telles que l'ID utilisateur, l'ID de groupe, le fuseau horaire et les arguments de la ligne de commande.
- **Remarque :** L'argument de la ligne de commande `--gameid devtest --port 30000` est facultatif.

## Réseau

Un réseau interne nommé `backend` est créé pour connecter les services PostgreSQL et pgAdmin.

## Volumes

Un volume nommé `db_data` est utilisé pour stocker les données de la base de données PostgreSQL.