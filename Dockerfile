FROM php:8.2-apache
RUN curl -OL https://github.com/composer/composer/releases/download/2.5.4/composer.phar
RUN mv composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer && a2enmod rewrite && service apache2 restart
RUN apt-get update && apt-get install -y nano
RUN apt-get install -y git

RUN git config --global user.email "enzo.maylin@etu.umontpellier.fr"

RUN git config --global user.name "Maylin-Capo"
RUN apt-get install -y wget
RUN docker-php-ext-install pdo_mysql
RUN apt-get install -y zip